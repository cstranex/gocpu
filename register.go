package main

import "fmt"

type accumulatorRegister struct {
	data byte
	output bool
	subtract bool

	carry bool
}

func newAccumulator() *accumulatorRegister {
	return &accumulatorRegister{}
}

func (accum *accumulatorRegister) op(aValue *byte, bValue *byte, bus *byte, flags *systemRegister) {
	if flags.input {
		flags.setBit(FlagCarry, accum.carry)
		flags.setBit(FlagZero, accum.data == 0)
	}
	if accum.output {
		*bus |= accum.data
	}
	if accum.subtract {
		accum.data = *aValue - *bValue
	} else {
		temp := uint16(*aValue) + uint16(*bValue)
		accum.carry = temp > 255
		accum.data = uint8(temp) // *aValue + *bValue
	}
}

type outputRegister struct {
	data byte
	input bool
	enable bool
}

func newOutput() *outputRegister {
	return &outputRegister{}
}

func (reg *outputRegister) op(bus *byte) {
	if reg.input {
		reg.data = *bus
	}

	if reg.enable {
		fmt.Print(">>> ", reg.data, "\n")
	}
}

type systemRegister struct {
	data   byte
	input  bool
	output bool
}

func newRegister() *systemRegister {
	return &systemRegister{}
}

func (reg *systemRegister) read() byte {
	return reg.data
}

func (reg *systemRegister) toBus(bus *byte) {
	if reg.output {
		*bus |= reg.data
	}
}

func (reg *systemRegister) fromBus(bus *byte) {
	if reg.input {
		reg.data = *bus
	}
}

func (reg *systemRegister) setBit(bit uint8, value bool) {
	var x byte = 0x01 << bit
	if !value {
		reg.data = reg.data & (0xff ^ x)
	} else {
		reg.data = reg.data | x
	}
}

func (reg *systemRegister) getBit(bit uint8) bool {
	return (reg.data >> bit) & 0x01 == 1
}

type programCounter struct {
	counter uint8
	enable bool
	output bool
	input bool
}

func newProgramCounter() *programCounter {
	return &programCounter{}
}

func (counter *programCounter) toBus(bus *byte) {
	if counter.output {
		*bus |= counter.counter
	}
}

func (counter *programCounter) fromBus(bus *byte) {
	if counter.enable {
		counter.counter++
	}

	if counter.input {
		counter.counter = *bus
	}
}