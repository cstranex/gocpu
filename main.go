package main

import (
	"os"
	"time"
)

// Microcode Opcodes
const XX  = 0x80000000  // End execution of the micro code (starts reading the next instruction)
const RS  = 0x40000000  // Reset Computer (Reset's all register and counter states)

/*
	Use NIF with FLC or FLZ to create conditional jump instructions.

	Specifying NIF inverts the criteria for conditional jump.

	Example:

	| NIF | FLC | NOP |
	-------------------
	|  1  |  0  |  1  |
	|  1  |  1  |  0  |
	|  0  |  0  |  0  |
	|  0  |  1  |  1  |
*/
const FLC = 0x200000 	// carry flag
const FLZ = 0x100000 	// zero flag
const NIF = 0x80000  	// No-Op if not set

const FI  = 0x40000		// CPU Flag Input
const FO  = 0x20000		// CPU Flag Output
const HL  = 0x10000   	// Halt
const MI  = 0x8000		// Memory Address In
const RI  = 0x4000		// RAM In
const RO  = 0x2000		// Ram Out
const II  = 0x1000		// Instruction In
const IO  = 0x0800		// Instruction Out
const AI  = 0x0400		// A Reg Input
const AO  = 0x0200		// A Reg Output
const EO  = 0x0100		// Sum Output
const SU  = 0x0080		// Subtract bit
const BI  = 0x0040		// B Reg Input
const BO  = 0x0020		// B Reg Output
const OE  = 0x0010		// Output Enable
const OI  = 0x0008		// Output Reg. Input
const CE  = 0x0004 		// Counter Enable (Increment Program Counter)
const CO  = 0x0002		// Counter Output
const CI  = 0x0001		// Counter Input

// CPU Instructions
const NOP = 0x00		// No-Op
const LOA = 0x01		// Load Immediate into Register A. Parameter = value
const LDA = 0x02		// Load Memory Data into Register A. Parameter = memory address to load from
const LOB = 0x03		// Load Immediate into Register B. Parameter = value
const LDB = 0x04		// Load Memory Data into Register B. Parameter = memory address to load from
const SOA = 0x05		// Store from Register A. Parameter = Memory Address to store into
const STA = 0x06		// Store from Register A. Parameter = memory address where address is located
const SOB = 0x07		// Store from Register B. Parameter = Memory Address to store into
const STB = 0x08		// Store from Register B. Parameter = memory address where address is located
const ADD = 0x09		// Add register A to register B and return the result into register A
const SUB = 0x0A		// Subtract register A from register B and return the result into register A
const JMP = 0x0B		// Unconditional Jump. Parameter = Memory address to jump to
const JMC = 0x0C		// Conditional Jump on Carry. Jump if carry flag is set. Parameter = Memory address to jump to
const JMZ = 0x0D		// Conditional Jump on Zero. Jump if zero flag is set. Parameter = Memory address to jump to
const OUT = 0x0E		// Output. Writes the value in register A to screen
const HLT = 0x0F		// Halt. Halts further execution of the program
const RST = 0x10		// Reset. Reset the computer's state including the program counter

// CPU Flags
const FlagCarry = 0x01	// Set if a math operation exceeds the length of register
const FlagZero = 0x00	// Set if a math operation results in a sum of 0


type systemRam struct {
	data            [255]byte	// 255 bytes of memory
	memoryAddress   uint8		// Current address of memory that is being read/written
	memoryAddressIn bool		// State of the memory address register
	input           bool		// state of the memory register accepting value changes
	output          bool		// state of the memory register outputing values
}

func (ram *systemRam) read() byte {
	return ram.data[ram.memoryAddress]
}

func newRam() *systemRam{
	return &systemRam{}
}

func (ram *systemRam) toBus(bus *byte) {
	if ram.output {
		*bus |= ram.data[ram.memoryAddress]
	}
}

func (ram *systemRam) fromBus(bus *byte) {
	if ram.memoryAddressIn {
		ram.memoryAddress = *bus
	}
	if ram.input {
		ram.data[ram.memoryAddress] = *bus
	}
}

func main() {

	/* These are the microcodes that make up the instructions of the CPU.*/
	microCode := [17][8]uint32{
		{CO|MI, RO|II|CE, XX, 0, 0, 0, 0, 0},                           	 // NOP
		{CO|MI, RO|II|CE, CO|MI, RO|MI, RO|AI|CE, XX, 0},               	 // LOA [memory address]
		{CO|MI, RO|II|CE, CO|MI, RO|AI|CE, XX, 0, 0},                   	 // LDA [value]
		{CO|MI, RO|II|CE, CO|MI, RO|MI, RO|BI|CE, XX, 0},               	 // LOB [memory address]
		{CO|MI, RO|II|CE, CO|MI, RO|BI|CE, XX, 0, 0},                   	 // LDB [value]
		{CO|MI, RO|II|CE, CO|MI, RO|MI, RI|AO|CE, XX, 0},               	 // SOA [memory address]
		{CO|MI, RO|II|CE, CO|MI, RI|AO|CE, XX, 0, 0},                   	 // STA [value]
		{CO|MI, RO|II|CE, CO|MI, RO|MI, RI|BO|CE, XX, 0},               	 // SOB [memory address]
		{CO|MI, RO|II|CE, CO|MI, RI|BO|CE, XX, 0, 0},                   	 // STB [value]
		{CO|MI, RO|II|CE, EO|AI, FI|XX, 0, 0, 0, 0},                    	 // ADD (a register and b register)
		{CO|MI, RO|II|CE, SU|EO|AI, FI|XX, 0, 0, 0, 0},                 	 // SUB (a register and b register)
		{CO|MI, RO|II|CE, CO|MI, RO|CI, XX, 0, 0, 0},               		 // JMP [memory address]
		{CO|MI, RO|II|CE, NIF|FLC|CO|MI, NIF|FLC|RO|CI, FLC|CE|XX, 0, 0, 0}, // JMC [memory address] (Jump on carry)
		{CO|MI, RO|II|CE, NIF|FLZ|CO|MI, NIF|FLZ|RO|CI, FLZ|CE|XX, 0, 0, 0}, // JMZ [memory address] (Jump if zero)
		{CO|MI, RO|II|CE, AO|OI, OE, XX, 0, 0},                         	 // OUT
		{CO|MI, RO|II|CE, HL, 0, 0, 0, 0, 0},                           	 // HLT
		{CO|MI, RO|II|CE, RS, 0, 0, 0, 0, 0},								 // RST
	}

	memory := newRam()

	/* An example program that adds numbers together until an overflow occurs and then restarts from the beginning */
	memory.data[0] = LOA  // Load from memory address
	memory.data[1] = 11   // Memory address
	memory.data[2] = LDB  // Load from value
	memory.data[3] = 1	  // value
	memory.data[4] = ADD  // Add the values
	memory.data[5] = JMC  // Jump on carry
	memory.data[6] = 10   // Address
	memory.data[7] = OUT  // Output the result
	memory.data[8] = JMP  // Jump
	memory.data[9] = 4 	  // Jump to address 4
	memory.data[10] = RST // Halt the program
	memory.data[11] = 220   // Value loaded by LDA

	instructionRegister := newRegister()
	aRegister := newRegister()
	bRegister := newRegister()
	accum := newAccumulator()
	output := newOutput()
	cpuFlags := newRegister()

	counter := newProgramCounter()
	var bus uint8 = 0

	// This is essentially our CPU clock
	cpuClock := time.NewTicker(10 * time.Millisecond)

	for {
		for i := 0; i < 8; i++ {
			<-cpuClock.C

			bus = 0
			op := microCode[instructionRegister.data][i]  // Current microcode op code

			// fmt.Printf("\nOP: %x, BUS: %x, INS: %x, PGM: %d, STP: %d\n", op, bus, instructionRegister.data, counter.counter, i)
			// fmt.Printf("   A: %x, B: %x, S: %x, F: %x, MA: %x, MV: %x\n", aRegister.data, bRegister.data, accum.data, cpuFlags.data, memory.memoryAddress, memory.read())

			if op & FLC == FLC && cpuFlags.getBit(FlagCarry) != (op & NIF == NIF) { // No-Op if carry bit set
				continue
			}

			if op & FLZ == FLZ && cpuFlags.getBit(FlagZero) != (op & NIF == NIF) { // No-Op if zero bit set
				continue
			}

			memory.memoryAddressIn = op & MI == MI
			memory.output = op & RO == RO
			memory.input = op & RI == RI

			counter.output = op & CO == CO
			counter.input = op & CI == CI
			counter.enable = op & CE == CE

			instructionRegister.input = op & II == II
			instructionRegister.output = op & IO == IO

			aRegister.input = op & AI == AI
			aRegister.output = op & AO == AO

			bRegister.input = op & BI == BI
			bRegister.output = op & BO == BO

			output.input = op & OI == OI
			output.enable = op & OE == OE

			accum.output = op & EO == EO

			cpuFlags.output = op & FO == FO
			cpuFlags.input = op & FI == FI

			accum.op(&aRegister.data, &bRegister.data, &bus, cpuFlags)

			memory.toBus(&bus)
			counter.toBus(&bus)
			instructionRegister.toBus(&bus)
			aRegister.toBus(&bus)
			bRegister.toBus(&bus)

			memory.fromBus(&bus)
			counter.fromBus(&bus)
			instructionRegister.fromBus(&bus)
			aRegister.fromBus(&bus)
			bRegister.fromBus(&bus)

			output.op(&bus)

			if op & HL == HL {
				os.Exit(0)
			}

			if op & RS == RS {
				// Reset the state of the machine
				aRegister.data = 0
				bRegister.data = 0
				counter.counter = 0
				instructionRegister.data = 0
				memory.memoryAddress = 0
				output.data = 0
				cpuFlags.data = 0
				accum.data = 0
				bus = 0
				break
			}

			if op & XX == XX {
				// End execution of the microcode
				break
			}

		}
	}


}
